#include "GrowduinoFirmware3.h"

#include <OneWire.h>
#include <DallasTemperature.h>
DallasTemp ds1(ONEWIRE_PIN);
DallasTemp ds2(ONEWIRE_PIN2);
#include <Wire.h>

void setup() {
  Serial.begin(115200);
}

void loop() {
  float vals1[3];
  float vals2[3];
  delay(1000);

  vals1[0] = sqrt(-1);
  vals1[1] = sqrt(-1);
  vals1[2] = sqrt(-1);
  vals2[0] = sqrt(-1);
  vals2[1] = sqrt(-1);
  vals2[2] = sqrt(-1);

  for(int i=0; i<3;i++){
    Serial.print("Measuring");
    ds1.measure(false);
    Serial.print(".");
    ds2.measure(true);
    Serial.println(".");
    Serial.print("Triple read 1: ");
    vals1[i] = ds1.triple_read();
    Serial.println(vals1[i]);
    Serial.print("Triple read 2: ");
    vals2[i] = ds2.triple_read();
    Serial.println(vals2[i]);
    delay(50);
  }
  Serial.println("Results: ");
  Serial.print("Dallas 1: ");
  Serial.println(return_middle(vals1));
  Serial.print("Dallas 2: ");
  Serial.println(return_middle(vals2));
  Serial.println(" ");
  Serial.println(" ");
  delay(300);
}
