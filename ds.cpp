#include "GrowduinoFirmware3.h"

/* Dallas onewire thermometers */

#include <OneWire.h>
#include <DallasTemperature.h>

DallasTemp::DallasTemp(int pin) {
  wire = new OneWire(pin);
  sensor = new DallasTemperature(wire);
  result = 0;
  do_search();
}

void DallasTemp::do_search() {
  result = wire->search(addr);
  if (result == 1) {
    if (OneWire::crc8( addr, 7) != addr[7]) {
      result = 0;

    } else {
      sensor->setResolution(11);
    }
  }
  wire->reset_search();
}

void DallasTemp::measure(bool waitForConversion = false) {
  do_search();
  if (result == 1) {
    sensor->setWaitForConversion(waitForConversion);
    sensor->requestTemperaturesByAddress(addr);
  }

}

float DallasTemp::inner_read() {
  float celsius = sensor->getTempC(addr);
  if (celsius < -126) return MINVALUE;
  if (celsius > 84) return MINVALUE;
  return celsius * 10;
}

float DallasTemp::read() {
  float retval;

  if (result == 0) {
    return MINVALUE;
  }

  for (int i=0; i<10; i++) {

    retval = inner_read();
    if (! isinf(retval)) {
      Serial.print("loop ");
      Serial.println(i);

      break;
      delay(50);
    }
  }
  return retval;
}

float DallasTemp::triple_read() {
  if (result == 0) {
    return MINVALUE;
  }
  float vals[3];
  vals[0] = this->read();
  delay(50);
  vals[1] = this->read();
  delay(50);
  vals[2] = this->read();
  return return_middle(vals);
}
