#include "GrowduinoFirmware3.h"
#include <math.h>
int analogReadAvg(int pin) {

  // introduce delay between mux switch and actual reading
  analogRead(pin);
  delay(ANALOG_READ_AVG_DELAY);
  analogRead(pin);

  long dataSum = 0L;
  int data;

  for (int i = 0; i < ANALOG_READ_AVG_TIMES; i++) {
    data = analogRead(pin);
    dataSum += data;

    delay(ANALOG_READ_AVG_DELAY);
  }

  int retval = (int) (dataSum / ANALOG_READ_AVG_TIMES);
  return retval;
}

int perThousand(int pin) {
  int retval;
  retval = analogReadAvg(pin);
  retval = map(retval, 0, 1024, 0, 1000);
  return retval;
}


float return_middle(float values[]) {
#ifdef WATCHDOG
  wdt_reset();
#endif

  // float values[] = {first_value, second_value, third_value};
  float tmpval;
  if (values[0] > values[1]) {
    tmpval = values[1];
    values[1] = values[0];
    values[0] = tmpval;
  }

  if (values[1] > values[2]) {
    tmpval = values[2];
    values[2] = values[1];
    values[1] = tmpval;
  }

  if (values[0] > values[1]) {
    tmpval = values[1];
    values[1] = values[0];
    values[0] = tmpval;
  }

  if (not(isinf(values[0]))) {
    return round(values[1]);  // middle value if there are not any -inftys (former MINVALUEs)
  }

  if (isinf(values[1])) {
    if (isinf(values[2])) { 
        return values[2];
    }
    return round(values[2]);  // last value if second is also -infty
  } else {
    return round((values[1] + values[2]) / 2);
  }
}
